package com.example.healthycalculator

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class RegisterActivity : AppCompatActivity() {

    private lateinit var buttonRegister: Button
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private  lateinit var editTextPassword2: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()

    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        buttonRegister = findViewById(R.id.buttonRegister)


    }
    private fun registerListeners() {
        buttonRegister.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val password2 = editTextPassword2.text.toString()

            if (email.isEmpty() || password.isEmpty() || password2.isEmpty()) {
                Toast.makeText(this,"Empty!!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if(password.length <9){
                Toast.makeText(this,"Please enter password minimum in 9 Char", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if(password.equals(password2)){
                Toast.makeText(this,"Both password are not matched", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.get(editTextEmail).toString()).matches()){
                Toast.makeText(this,"Please enter valid Mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(this,"Error!",Toast.LENGTH_SHORT).show()
                    }

                }

}