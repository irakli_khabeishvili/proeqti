package com.example.healthycalculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button
    private lateinit var buttonRegister: Button
    private lateinit var buttonForgotPassword: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonLogin = findViewById(R.id.buttonLogin)
        buttonRegister = findViewById(R.id.buttonRegister)
        buttonForgotPassword = findViewById(R.id.buttonForgotPassword)

        buttonLogin.setOnClickListener(){
            startActivity(Intent(this,ProfileActivity::class.java)
                .putExtra("E-mail",editTextEmail.text.toString())
                .putExtra("password",editTextPassword.text.toString()))



        }

        buttonLogin.setOnClickListener(){
            if(editTextEmail.text.isNullOrBlank()&&editTextPassword.text.isNullOrBlank()){
                Toast.makeText(this,"Please fill the required field",Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(this,"${editTextEmail.text} is logged in",Toast.LENGTH_SHORT)
            }
        }


        FirebaseAuth.getInstance()
            .createUserWithEmailAndPassword(editTextEmail,editTextPassword)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(this,"Error!",Toast.LENGTH_SHORT).show()
                }

            }





    }

}