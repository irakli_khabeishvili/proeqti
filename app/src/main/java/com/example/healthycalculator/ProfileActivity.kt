package com.example.healthycalculator

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import java.math.BigDecimal
import java.math.RoundingMode

class ProfileActivity : AppCompatActivity() {

    private lateinit var alculateBtn: Button
    private lateinit var heightEDTX: EditText
    private lateinit var weightEDTX : EditText
    private lateinit var container: LinearLayout



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        alculateBtn.setOnClickListener {
            if (heightEDTX.text.isNotEmpty() && weightEDTX.text.isNotEmpty()) {
                val weight = weightEDTX.text.toString().toDouble()
                val height = heightEDTX.text.toString().toDouble() / 100

                if (weight > 0 && weight < 600 && height >= 0.50 && height < 2.5) {
                    val intent = Intent(this@ProfileActivity, ResultActivity::class.java)
                    intent.putExtra("bmi", calculateBMI(weight, height))
                    startActivity(intent)
                } else {
                    showError("Incorrect Values")
                }
            } else {
                showError("A field is missing")
            }
        }

    }
    private fun showError(errorMsg: String) {
        val snackbar = Snackbar.make(container, "error: $errorMsg !", Snackbar.LENGTH_LONG)
        snackbar.view.setBackgroundResource(R.color.Red)
        snackbar.show()
    }

    private fun calculateBMI(weight: Double, height: Double) = BigDecimal(weight/ (height * height))
        .setScale(2,RoundingMode.HALF_EVEN).toDouble()
}