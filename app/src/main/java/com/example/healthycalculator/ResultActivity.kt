package com.example.healthycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

class ResultActivity : AppCompatActivity() {

    private lateinit var containerR: LinearLayout
    private lateinit var bmiValueTV: TextView
    private lateinit var bmiFlagImageView: ImageView
    private lateinit var bmiLabelTv: TextView
    private lateinit var commentTv: TextView
    private lateinit var advice1IMG: ImageView
    private lateinit var advice1TV: TextView
    private lateinit var advice2IMG: ImageView
    private lateinit var advice2TV: TextView
    private lateinit var advice3IMG: ImageView
    private lateinit var advice3TV: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val bmi = intent.getDoubleExtra("bmi", -1.0)
        if (bmi == -1.0){
            containerR.visibility = View.GONE

        }
        else{
            bmiValueTV.text = bmi.toString()
            if (bmi < 18.5){
                containerR.setBackgroundResource(R.drawable.yellow_bg)
                bmiFlagImageView.setImageResource(R.drawable.exclamation)
                bmiLabelTv.text = "You have an UnderWeight !"
                commentTv.text = "Here are some advices to help you increase your weight"
                advice1IMG.setImageResource(R.drawable.water)
                advice1TV.text = "Don't drink water before meals"
                advice2IMG.setImageResource(R.drawable.eat)
                advice2TV.text = "Eat more"
                advice3IMG.setImageResource(R.drawable.sleep)
                advice3TV.text = "Get quality sleep"
            }else{
                if (bmi > 25){
                    containerR.setBackgroundResource(R.drawable.red_bg)
                    bmiFlagImageView.setImageResource(R.drawable.warning)
                    bmiLabelTv.text = "You have an OverWeight !"
                    commentTv.text = "Here are some advices to help your decrease your weight"
                    advice1IMG.setImageResource(R.drawable.water)
                    advice1TV.text = "Don't drink water a half hour before meals"
                    advice2IMG.setImageResource(R.drawable.eggs)
                    advice2TV.text = "Eat only two meals per day and make sure that they contains a high protein"
                    advice3IMG.setImageResource(R.drawable.sugar)
                    advice3TV.text = "Drink coffee or tea avoid sugary food"
                }
            }

        }

    }
}